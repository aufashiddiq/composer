import 'package:app_name/helpers/constants.dart';
import 'package:flutter/material.dart';

class NavigationDrawer extends StatelessWidget {

  final List<String> menuList;
  final int selectedMenu;
  final Function onChangeMenu;

  NavigationDrawer(this.menuList, this.selectedMenu, this.onChangeMenu);

  @override
  Drawer build(BuildContext context) {
    return Drawer(
      child: Container(
        color: blackColor,
        padding: EdgeInsets.all(mediumSpacing),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Flexible(
              flex: 1,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: menuList.map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                        margin: EdgeInsets.symmetric(vertical: smallSpacing),
                        child: FlatButton(
                          onPressed: () {
                            onChangeMenu(menuList.indexOf(i));
                            Scaffold.of(context).openEndDrawer();
                          }, 
                          child: Text(i, style: TextStyle(
                            fontFamily: 'ProductSansBold',
                            fontSize: mediumTextSize,
                            color: menuList[selectedMenu] == i ? primaryColor : whiteColor,
                          )
                        ))
                      );
                    }
                  );
                }).toList(),
              ),
            )
            ,
            Container(
              margin: EdgeInsets.symmetric(vertical: smallSpacing),
              child: FlatButton(
                onPressed: () {
                  Scaffold.of(context).openEndDrawer();
                }, 
                child: Text('Logout', style: TextStyle(
                  fontFamily: 'ProductSansBold',
                  fontSize: mediumTextSize,
                  color: whiteColor,
                )
              ))
            ),
          ],
        )
      ),
    );
  }
}