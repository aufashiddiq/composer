import 'package:flutter/material.dart';
import '../helpers/constants.dart';

class CustomTextField extends StatelessWidget {

  @required final textFieldController;
  @required final label;
  @required final textInputType;
  final textInputAction;

  CustomTextField({
    this.textFieldController, 
    this.label, 
    this.textInputType, 
    this.textInputAction
  });

  @override
  Widget build(BuildContext context) {
    return TextField(
      keyboardType: textInputType,
      obscureText: textInputType == TextInputType.visiblePassword ? true : false,
      textInputAction: textInputAction != null ? textInputAction : TextInputAction.done,
      decoration: InputDecoration(
        hintText: label,
        fillColor: whiteColor,
        hintStyle: TextStyle(color: greyColor, fontFamily: 'ProductSans'),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(regularBorderRadius),
          borderSide: BorderSide(style: BorderStyle.none, width: 0),
          gapPadding: smallSpacing,
        ),
        filled: true,
        contentPadding: EdgeInsets.symmetric(horizontal: regularSpacing)
      ),
      style: TextStyle(
        fontSize: regularTextSize, 
        color: blackColor,
        fontFamily: 'ProductSans',
      ),
      controller: textFieldController,
    );
  }
}