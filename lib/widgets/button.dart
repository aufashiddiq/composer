import 'package:flutter/material.dart';
import '../helpers/constants.dart';

class CustomButton extends StatelessWidget {

  final String text;
  final onPress;

  CustomButton({this.text, this.onPress});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: onPress, 
      child: Text(text, style: TextStyle(fontSize: regularTextSize, fontFamily: 'ProductSansBold')),
      color: primaryColor,
      padding: EdgeInsets.all(regularSpacing),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(regularBorderRadius),
        side: BorderSide(style: BorderStyle.none, width: 0),
      ),
    );
  }
}