import '../helpers/constants.dart';
import 'package:flutter/material.dart';

class Header extends StatelessWidget {

  final String title;
  Header(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(top: smallSpacing, bottom: smallSpacing, right: mediumSpacing),
      margin: EdgeInsets.only(top: veryLargeSpacing),
      child: Row(
        children: <Widget>[
          FlatButton(
            onPressed: () => Scaffold.of(context).openDrawer(), 
            child: Icon(Icons.menu, color: blackColor, size: mediumIconSize),
          ),
          Container(
            child: Text(title, style: TextStyle(
              color: blackColor,
              fontSize: largeTextSize,
              fontFamily: 'ProductSansBold',
            )),
          )
        ],
      ),
    );
  }
}