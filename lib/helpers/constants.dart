import 'package:flutter/painting.dart';
// import 'package:flutter/material.dart';

// Constant for App Spacing
const verySmallSpacing = 6.0;
const smallSpacing = 10.0;
const regularSpacing = 14.0;
const mediumSpacing = 18.0;
const largeSpacing = 22.0;
const veryLargeSpacing = 26.0;

// Border Radius 
const smallBorderRadius = 5.0;
const regularBorderRadius = 10.0;


// Colors
const primaryColor = Color(0xFFF2C94C);
const lightGreyColor = Color(0xFFF2F2F2);
const whiteColor = Color(0xFFFFFFFF);
const blackColor = Color(0xFF444444);
const greyColor = Color(0xFFBDBDBD);

// Text Size
const verySmallTextSize = 8.0;
const smallTextSize = 12.0;
const regularTextSize = 16.0;
const mediumTextSize = 20.0;
const largeTextSize = 24.0;
const veryLargeTextSize = 30.0;

// Icon Size
const smallIconSize = 16.0;
const regularIconSize = 24.0;
const mediumIconSize = 32.0;
const largeIconSize = 40.0;
