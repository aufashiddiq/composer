import '../helpers/constants.dart';
import '../widgets/header.dart';
import '../widgets/navigation_drawer.dart';
import 'package:flutter/material.dart';

class MainController extends StatefulWidget {
  @override
  _MainControllerState createState() => _MainControllerState();
}

class _MainControllerState extends State<MainController> {

  final List<String> menuList = ['Compositions', 'Materials', 'History'];
  int selectedMenu = 0;

  void _changeMenu (index) {

    setState(() {
      selectedMenu = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: whiteColor,
        child: Column(
          children: <Widget>[
            Header(menuList[selectedMenu])
          ],
        ),
      ),
      drawer: NavigationDrawer(menuList, selectedMenu, _changeMenu),
    );
  }
}