import 'package:app_name/controllers/main_controller.dart';
import 'package:app_name/screens/auth/login_screen.dart';
import 'package:flutter/material.dart';

class LoginController extends StatefulWidget {
  @override
  _LoginControllerState createState() => _LoginControllerState();
}

class _LoginControllerState extends State<LoginController> {

  final usernameController =  TextEditingController();
  final passwordController = TextEditingController();

  void _signInAction() {

    if (usernameController.text == 'user' && passwordController.text == "pass") {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => MainController()
      ));
    } else {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Username atau Password salah!"),
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return LoginScreen(usernameController, passwordController, _signInAction);
  }
}