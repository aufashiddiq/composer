import '../../helpers/constants.dart';
import '../../widgets/button.dart';
import '../../widgets/textfield.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {

  final usernameController;
  final passwordController;
  final signInAction;

  LoginScreen(this.usernameController, this.passwordController, this.signInAction);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: lightGreyColor,
      padding: EdgeInsets.all(mediumSpacing),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Container(
            padding: EdgeInsets.only(bottom: veryLargeSpacing),
            child: Text(
              'Composer', 
              style: TextStyle(
                color: blackColor, 
                fontSize: veryLargeTextSize,
                fontFamily: 'ProductSansBold'
              ),
            ),
          ),
          
          Container(
            padding: EdgeInsets.only(bottom: mediumSpacing),
            child: Text(
              'Login', 
              style: TextStyle(
                color: blackColor, 
                fontSize: mediumTextSize,
                fontFamily: 'ProductSans'
              ),
            ),
          ),

          Container(
            padding: EdgeInsets.only(bottom: mediumSpacing),
            child: CustomTextField(
              textFieldController: usernameController, 
              label: 'Email/Username',
              textInputType: TextInputType.emailAddress,
            ),
          ),
          
          Container(
            padding: EdgeInsets.only(bottom: largeSpacing),
            child: CustomTextField(
              textFieldController: passwordController, 
              label: 'Password',
              textInputType: TextInputType.visiblePassword,
            ),
          ),

          CustomButton(text: 'Sign In', onPress: signInAction,)
          
        ]
      ),
    );
  }

}
